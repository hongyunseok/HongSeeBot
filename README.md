# HongSeeBot
텔레그램 봇 API 활용한 급식 알리미
급식 경우 교육청 사이트에서 크롤링해서 가져옵니다.

# Main.py
해당 파일에서 수정할 점은 아래와 같습니다.

29번: TOKEN = "<GET TOKEN>"
// 토큰 넣어주세요

14, 39번: username = 000000000
// 본인의 텔레그램 채팅방 아이디를 넣어주세요.

# School.py
해당 파일에서 수정할 점은 아래와 같습니다.

17번: regioncode = '<학교가 속한 교육청 사이트 주소)'
// 해당 학교의 지역 교육청 주소를 입력해주세요.
// ex) 경기도 = goe.go.kr

18번: schulcode = '<학교 코드>'
// 학교 코드를 아래 링크 참고하여 구해옵니다.
// 'https://www.meatwatch.go.kr/biz/bm/sel/schoolListPopup.do'

# 라이브러리
datetime, telepot, python-dateutil, urlopen, BeautifulSoup4, lxml

# 장점
평일 8시마다 급식을 알려줍니다.

# 단점
속도가 매우 느립니다. (데이터 베이스에서 저장안함)
-> 말 그대로 저장안하고 그대로 구해와서 보냅니다.
